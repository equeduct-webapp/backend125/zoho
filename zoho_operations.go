package zoho

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"net/http"

	"equeduct.com/module/data"
	"equeduct.com/module/util"
)

// insertZoho: bind information and post json to zoho api
func InsertZoho(formDetails data.UserDetails) {

	fmt.Println("refreshing zoho token..")
	zohoRefreshAccessToken()

	if data.ZOHO_ACCESS_TOKEN == "" {
		return
	}

	headers := map[string]string{}
	headers["Authorization"] = "Zoho-oauthtoken " + data.ZOHO_ACCESS_TOKEN
	url := "https://www.zohoapis.com/crm/v2/leads"

	var recordJsonData data.RecordZoho
	var recordJson data.InsertRecordsData

	// binding the information from input json to the json to be sent to zoho api

	// bind company info
	//CompanyZipCode := strconv.Itoa(formDetails.Companies.CompanyZipCode)
	CompanyZipCode := fmt.Sprintf("%v", formDetails.Companies.CompanyZipCode)

	recordJsonData.Company = formDetails.Companies.CompanyLegalName
	recordJsonData.PlanName = formDetails.Companies.PlanName
	//recordJsonData.LeadOwner = "Equeduct"
	recordJsonData.Street = formDetails.Companies.CompanyStreetAddress
	recordJsonData.City = formDetails.Companies.CompanyCity
	recordJsonData.State = formDetails.Companies.CompanyState
	recordJsonData.ZipCode = CompanyZipCode
	recordJsonData.Country = formDetails.Companies.CompanyCountry
	recordJsonData.Phone = formDetails.Companies.CompanyPhoneNumber
	recordJsonData.DateBusinessStarted = formDetails.Companies.CompanyStartDate
	recordJsonData.CompanyTaxIDein = formDetails.Companies.CompanyTaxIDEin

	// bind DBA info
	recordJsonData.NameDBA = formDetails.Companies.DbaName
	recordJsonData.StreetAddressDBA = formDetails.Companies.DbaStreetAddress
	recordJsonData.CityDBA = formDetails.Companies.DbaCity
	recordJsonData.StateDBA = formDetails.Companies.DbaState
	recordJsonData.ZipcodeDBA = formDetails.Companies.DbaZipCode
	recordJsonData.CountryDBA = formDetails.Companies.DbaCountry

	// bind owner info
	recordJsonData.Email = formDetails.User.OwnerEmailAddress
	recordJsonData.Mobile = formDetails.User.OwnerPhoneNumber
	// recordJsonData.OwnerName = formDetails.CompanyCity
	recordJsonData.FirstName = formDetails.User.OwnerFirstName
	recordJsonData.LastName = formDetails.User.OwnerLastName
	recordJsonData.OwnerDob = formDetails.User.OwnerDob
	recordJsonData.OwnerEmail = formDetails.User.OwnerEmailAddress
	recordJsonData.OwnerStreetAddress = formDetails.User.OwnerStreetAddress
	recordJsonData.OwnerCity = formDetails.User.OwnerCity
	recordJsonData.OwnerState = formDetails.User.OwnerState
	recordJsonData.OwnerZipCode = formDetails.User.OwnerZipCode
	recordJsonData.OwnerCountry = formDetails.User.OwnerCountry
	recordJsonData.OwnershipPercentage = formDetails.User.OwnerPercentage
	recordJsonData.OwnerDriverLicenseNumber = formDetails.User.OwnerDriverLicenseNumber
	recordJsonData.OwnerDriverLicenseIssueDate = formDetails.User.OwnerDriverLicenseIssueDate
	recordJsonData.OwnerDriverLicenseExpirationDate = formDetails.User.OwnerDriverLicenseExpirationDate
	recordJsonData.OwnerSocialSecurityNumber = formDetails.User.OwnerSocialSecurityNumber
	// recordJsonData.Lead_Name = formDetails.User.OwnerName

	fmt.Println("Zoho data is ready for upload")

	recordJson.Data = []data.RecordZoho{recordJsonData}
	trigger := []string{"approval", "workflow", "blueprint"}
	recordJson.Trigger = trigger
	rawJson, err := json.Marshal(recordJson)
	//fmt.Println(string(rawJson))
	if err != nil {
		fmt.Println("Unable to marshall the record")
		return
	}

	response, statuscode, err := util.RequestAPIData("POST", url, string(rawJson), headers)
	if err != nil || statuscode != 201 {
		fmt.Println("could not Insert the record")
		return
	}

	var responseObj data.LeadsResponse

	err = json.Unmarshal(response, &responseObj)
	if err != nil {
		fmt.Println("Error unmarshalling leads api response ", err)
	}

	if len(responseObj.Data) > 0 {
		leadID := responseObj.Data[0].Details.ID
		if responseObj.Data[0].Status == "success" {
			var frontIDName, backIDName string
			frontID, format := util.Base64StringToBytes(formDetails.IdentificationFrontSide)
			if len(frontID) > 0 && format != "" {
				frontIDName = formDetails.IdentificationFrontSideName
			}
			backID, format := util.Base64StringToBytes(formDetails.IdentificationBackSide)
			if len(backID) > 0 && format != "" {
				backIDName = formDetails.IdentificationBackSideName
			}

			if frontIDName != "" {
				zohoFileUploader(frontID, frontIDName, leadID)
			}
			if backIDName != "" {
				zohoFileUploader(backID, backIDName, leadID)
			}

			fmt.Println("Zoho data uploaded successfully")
		}
	}
}

// zohoRefreshingAccessToken: this will refresh the access token
func zohoRefreshAccessToken() {
	if data.ZOHO_REFESH_TOKEN == "" || data.ZOHO_CRED.ClientID == "" || data.ZOHO_CRED.ClientSecret == "" {
		fmt.Println("zoho not initialized, could not refresh access token")
		return
	}
	headers := map[string]string{}
	url := fmt.Sprintf("https://accounts.zoho.com/oauth/v2/token?refresh_token=%s&client_id=%s&client_secret=%s&grant_type=refresh_token",
		data.ZOHO_REFESH_TOKEN, data.ZOHO_CRED.ClientID, data.ZOHO_CRED.ClientSecret)

	body, statuscode, err := util.RequestAPIData("POST", url, "", headers)
	var refAccessTokenResp data.RefreshTheAccessToken
	err = json.Unmarshal(body, &refAccessTokenResp)
	if err != nil || statuscode != 200 {
		fmt.Println("could not refresh the access token")
		return
	}

	data.ZOHO_ACCESS_TOKEN = refAccessTokenResp.AccessToken
}

func zohoFileUploader(file []byte, fileName, leadID string) {

	url := "https://www.zohoapis.com/crm/v2/Leads/" + leadID + "/Attachments"
	method := "POST"

	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)

	frontFile, err := writer.CreateFormFile("file", fileName)
	if err != nil {
		fmt.Println(err)
		return
	}
	n, err := frontFile.Write(file)
	if err != nil {
		fmt.Println(n, err)
		return
	}

	err = writer.Close()
	if err != nil {
		fmt.Println(err)
		return
	}

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)
	if err != nil {
		fmt.Println(err)
		return
	}
	req.Header.Add("Authorization", "Zoho-oauthtoken "+data.ZOHO_ACCESS_TOKEN)

	req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer res.Body.Close()

	// for reading the response json
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(body))
}
